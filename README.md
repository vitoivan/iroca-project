# iRoça App (V1.0)

### _Status do Projeto:_ 🚧 **Aguardando um Back-End real...** 🚧

---
#### [----->>> Site <<<-----](https://iroca-project.vercel.app/)
---

### **Este projeto é uma idealização do Wesley da Costa, adotada como projeto de fim do módulo de **_#frontEnd_**, do curso de **_#fullStack_\*\* da [Kenzie Academy Brasil](https://kenzie.com.br/).

### Apresentação:

- ### Descrição:
  - O **_iRoça_** é um aplicativo destinado permitir que o pequeno produtor rural se cadastre e exponha todos os seus produtos, com os respectivos preços e quantidades disponíveis no momento, ganhando mais visibilidade aumentando sua capacidade de negociação e venda.
  - Mais detalhes neste [**_PDF_**](https://drive.google.com/file/d/1SvS6R2T0su4XplLWAki5sIecs5QOaOou/view?usp=sharing)

### Tecnologias aplicadas:

- Linguagem aplicada: [**_TypeScript_**](https://www.typescriptlang.org/) ;
- Versionamento via [**_Git_**](https://git-scm.com/) ;
- Conhecimentos intermediário em [**_React_**](https://reactjs.org/) e diversas outras libs auxiliares; e
- Autenticação via _"fakeAPI"_ ([**_JSON Server_**](https://www.npmjs.com/package/json-server) + [**_JSON Server Auth_**](https://www.npmjs.com/package/json-server-auth)).

### Demonstração:

Vide vídeo apresentação no seguinte [link]().

---

---

### Team:

<a href="https://www.linkedin.com/in/heitor-rezende">
 <img style="border-radius: 50%;" src="https://secure.gravatar.com/avatar/97cf0329a788a80037d1c10b7e85d0de?s=800&d=identicon" width="90px;" alt="Wesley da Costa"/>
 <br />
 <sub><b>Heitor Rezende</b></sub></a>

[![GitLab Badge](https://img.shields.io/badge/-Heitor_Rezende-black?style=plastic&logo=GitLab&logoColor=yellow&link=https://gitlab.com/heitorrezende)](https://gitlab.com/heitorrezende)
[![Linkedin Badge](https://img.shields.io/badge/-Heitor_Rezende-blue?style=plastic&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/heitor-rezende)](https://www.linkedin.com/in/heitor-rezende)

<a href="https://www.linkedin.com/in/marconemm/">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/15804964?s=400&amp;u=60f45399d863c1410217fc6666bc628c43f554dd&amp;v=4" width="90px;" alt="Marcone Melo"/>
 <br />
 <sub><b>Marcone Melo</b></sub></a>

[![GitLab Badge](https://img.shields.io/badge/-Marcone_Melo-black?style=plastic&logo=GitLab&logoColor=yellow&link=https://gitlab.com/marconemm)](https://gitlab.com/marconemm)
[![GitHub Badge](https://img.shields.io/badge/-Marcone_Melo-black?style=plastic&logo=GitHub&logoColor=white&link=https://github.com/marconemm)](https://github.com/marconemm)
[![Linkedin Badge](https://img.shields.io/badge/-Marcone_Melo-blue?style=plastic&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/marconemm/)](https://www.linkedin.com/in/marconemm/)
[![Yahoo Badge](https://img.shields.io/badge/-marconemendonca@ymail.com-c14438?style=plastic&logo=Yahoo!&logoColor=white&link=mailto:marconemendonca@ymail.com)](mailto:marconemendonca@ymail.com)

<a href="https://www.linkedin.com/in/victor-ivan">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/17788366" width="90px;" alt="Wesley da Costa"/>
 <br />
 <sub><b>Victor Ivan (Daddy, The)</b></sub></a>

[![GitHub Badge](https://img.shields.io/badge/-Victor_Ivan-black?style=plastic&logo=GitHub&logoColor=white&link=https://github.com/vitoivan/)](https://github.com/vitoivan/)
[![Linkedin Badge](https://img.shields.io/badge/-Victor_Ivan-blue?style=plastic&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/victor-ivan)](https://www.linkedin.com/in/victor-ivan)

<a href="https://www.linkedin.com/in/wesleydcm/">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/71963380" width="90px;" alt="Wesley da Costa"/>
 <br />
 <sub><b>Wesley da Costa</b></sub></a>

[![GitLab Badge](https://img.shields.io/badge/-Wesley_da_Costa-black?style=plastic&logo=GitLab&logoColor=yellow&link=https://gitlab.com/wesleydcm)](https://gitlab.com/wesleydcm)
[![GitHub Badge](https://img.shields.io/badge/-Wesley_da_Costa-black?style=plastic&logo=GitHub&logoColor=white&link=https://github.com/wesleydcm)](https://github.com/wesleydcm)
[![Linkedin Badge](https://img.shields.io/badge/-Wesley_da_Costa-blue?style=plastic&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/wesleydcm/)](https://www.linkedin.com/in/wesleydcm/)

<a href="https://www.linkedin.com/in/mcromao/">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/89869004" width="90px;" alt="Marcelo Romão"/>
 <br />
 <sub><b>Marcelo Romão</b></sub></a>

 [![GitLab Badge](https://img.shields.io/badge/-Marcelo_Romão-black?style=plastic&logo=GitLab&logoColor=yellow&link=https://gitlab.com/MarceloRomao)](https://gitlab.com/MarceloRomao)
[![GitHub Badge](https://img.shields.io/badge/-Marcelo_Romão-black?style=plastic&logo=GitHub&logoColor=white&link=https://github.com/mcromao)](https://github.com/mcromao)
[![Linkedin Badge](https://img.shields.io/badge/-Marcelo_Romão-blue?style=plastic&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/mcromao/)](https://www.linkedin.com/in/mcromao/)

